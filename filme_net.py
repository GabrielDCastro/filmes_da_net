import requests
import json

def requisicao(titulo):
    try:
        req = requests.get('http://theapache64.com/movie_db/search?keyword=' + titulo)
        dicionario = json.loads(req.text)
        return dicionario
    except:
        print('Error na conexão')
        return None

def printar_detalhes(filme):
    print('\033[34mTítulo: {}\33[m'.format(filme['data']['name']))
    print('\033[34mEnredo: {}\33[m'.format(filme['data']['plot']))
    print('\033[34mAno: {}\33[m'.format(filme['data']['year']))
    print('\033[34mAtores: {}\33[m'.format(filme['data']['stars']))
    print('\33[34mDiretor: {}\33[m'.format(filme['data']['director']))
    print('\33[34mGenero: {}\33[m'.format(filme['data']['genre']))

sair = False
while not sair:
    op = input('Escreva o nome de um filme: ou SAIR para fechar o programa\n')

    if op == "SAIR":
        sair = True
    else:
        filme = requisicao(op)

        if filme['error_code'] == 1:
            print("Filme não encontrado")
        else:
            printar_detalhes(filme)

